
# HackCast Mobile Client Prototype

HackCast is a "zero-to-one" take on podcast player app.

The main features that differentiate HackCast player from its competitors are:
- Built-in show section timestamp integration, so you no longer need to hunt for the show notes and manually seek to a specific timestamp.
- Ability to show particular text, link, pictures, and GIFs at specific timestamps.
- Built-in editor to add sections, images, links, GIFs to trigger at specific timestamps.

All of these new features are made possible by a simple JSON file to accompany the audio file that we validate and store on [HackCast's backend server](https://gitlab.com/hackcast/hackcast-backend/).

## Demo & Screenshots

You can watch the demo at [https://www.loom.com/share/bb887dd0903b4e8bac7778d75bb62dce](https://www.loom.com/share/bb887dd0903b4e8bac7778d75bb62dce)

![Screenshot 1](https://i.imgur.com/061KZun.png)
![Screenshot 2](https://i.imgur.com/APXARkH.png)
![Screenshot 3](https://i.imgur.com/UjgBXrJ.png)