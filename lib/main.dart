import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hackcast_app/screens/home/home_screen.dart';

void main() => runApp(HackCastApp());

class HackCastApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    return MaterialApp(
      initialRoute: "/",
      routes: {
        "/": (context) => HomeScreen(),
      },
      title: 'HackCast',
      theme: ThemeData(
        fontFamily: "Rubik",
        primarySwatch: Colors.blue,
      ),
    );
  }
}
