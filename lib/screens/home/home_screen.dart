import 'dart:ui';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';
import 'widgets/media_player.dart';
import 'widgets/section_editor.dart';
import 'widgets/canvas_editor.dart';
import 'widgets/section_list.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  AudioPlayer player;
  Map<String, dynamic> contentSections;
  Map<String, dynamic> triggers;
  Map<String, dynamic> lolMediaData;
  SolidController _bottomSheetController = SolidController();
  double _bottomSheetMaxHeight;
  BehaviorSubject<int> sectionStream = BehaviorSubject<int>();
  PublishSubject<Duration> audioDurationStream = PublishSubject<Duration>();
  BehaviorSubject<Duration> audioPositionStream = BehaviorSubject<Duration>();
  PublishSubject<AudioPlayerState> audioPlayerStateStream =
      PublishSubject<AudioPlayerState>();
  PublishSubject<Widget> interactivityObjectStream = PublishSubject<Widget>();
  BehaviorSubject<Map<String, dynamic>> contentSectionsStream =
      BehaviorSubject<Map<String, dynamic>>();
  BehaviorSubject<Map<String, dynamic>> triggersStream =
      BehaviorSubject<Map<String, dynamic>>();
  BehaviorSubject<bool> creatorModeStream = BehaviorSubject<bool>();
  BehaviorSubject<bool> creatorModeTypeStream = BehaviorSubject<bool>();
  BehaviorSubject<Map<String, dynamic>> mediaDataStream =
      BehaviorSubject<Map<String, dynamic>>();

  List<TextEditingController> _sectionNameControllers = List();
  List<TextEditingController> _sectionTimestampControllers = List();
  List<TextEditingController> _triggerTypeControllers = List();
  List<TextEditingController> _triggerContentControllers = List();
  List<TextEditingController> _triggerTimestampControllers = List();

  String mediaDataUrl = "http://138.68.25.217:8080/podcasts/hacklodge/p1";
  String updateUrl = "http://138.68.25.217:8080/podcasts/update";

  void initContentControllers() {
    _sectionNameControllers = List();
    _sectionTimestampControllers = List();
    for (int i = 0; i < contentSections.keys.length; i++) {
      _sectionNameControllers.add(TextEditingController());
      _sectionTimestampControllers.add(TextEditingController());

      _sectionNameControllers[i].text =
          contentSections[contentSections.keys.toList()[i]];

      _sectionTimestampControllers[i].text = Duration(
        seconds: int.parse(
          contentSections.keys.toList()[i],
        ),
      ).toString().split('.').first.padLeft(8, "0");
    }
  }

  void initTriggerControllers() {
    _triggerTypeControllers = List();
    _triggerContentControllers = List();
    _triggerTimestampControllers = List();

    for (int i = 0; i < triggers.keys.length; i++) {
      _triggerTypeControllers.add(TextEditingController());
      _triggerContentControllers.add(TextEditingController());
      _triggerTimestampControllers.add(TextEditingController());

      dynamic trigger = triggers[triggers.keys.toList()[i]];

      if (trigger['type'] == "display_text") {
        _triggerContentControllers[i].text = trigger['text'];
      } else if (trigger['type'] == "display_image") {
        _triggerContentControllers[i].text = trigger['resource'];
      }

      _triggerTimestampControllers[i].text = Duration(
        seconds: int.parse(
          triggers.keys.toList()[i],
        ),
      ).toString().split('.').first.padLeft(8, "0");

      _triggerTypeControllers[i].text = trigger['type'];
    }
  }

  Future<http.Response> updateMediaData() async {
    var body = json.encode(lolMediaData);
    var response = await http.post(updateUrl,
        headers: {"Content-Type": "application/json"}, body: body);
    return response;
  }

  @override
  void initState() {
    player = AudioPlayer();

    _fetchMediaData();

    mediaDataStream.stream.listen((mediaData) {
      lolMediaData = Map.from(mediaData);

      contentSections = Map.from(mediaData['contents']);
      contentSectionsStream.add(contentSections);
      initContentControllers();

      triggers = Map.from(mediaData['triggers']);
      triggersStream.add(Map.from(mediaData['triggers']));
      initTriggerControllers();
    });

    creatorModeStream.stream.listen((creatorMode) async {
      if (creatorMode == false) {
        var res = (await updateMediaData()).statusCode;
        while (res != 200) {
          res = (await updateMediaData()).statusCode;
        }
      }
    });

    contentSectionsStream.stream.listen((_contentSections) {
      contentSections = Map.from(_contentSections);
      initContentControllers();
    });

    triggersStream.stream.listen((_triggers) {
      triggers = Map.from(_triggers);
      initTriggerControllers();
    });

    player.onDurationChanged.listen((Duration d) {
      print('Max duration: $d');
      audioDurationStream.add(d);
    });
    player.onAudioPositionChanged.listen((Duration p) async {
      print('Current position: $p');
      int currentSection;
      List<String> contentTimestamps = contentSections.keys.toList();
      List<String> triggerTimestamps = triggers.keys.toList();

      for (int i = 0; i < contentTimestamps.length; i++) {
        if (p >= Duration(seconds: int.parse(contentTimestamps[i]))) {
          currentSection = i;
        }
      }

      for (int i = 0; i < triggerTimestamps.length; i++) {
        if (p >= Duration(seconds: int.parse(triggerTimestamps[i]))) {
          Map<String, dynamic> trigger = triggers[triggerTimestamps[i]];

          Widget _interactivityObject;

          if (trigger['type'] == "display_text") {
            _interactivityObject = Text(
              trigger['text'],
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            );
          } else if (trigger['type'] == "display_image") {
            _interactivityObject = Image.network(trigger['resource']);
          }

          interactivityObjectStream.add(_interactivityObject);
        }
      }

      audioPositionStream.add(p);
      sectionStream.add(currentSection);
    });
    player.onPlayerStateChanged.listen((AudioPlayerState s) {
      print('Current widget.player state: $s');
      audioPlayerStateStream.add(s);
    });

    super.initState();
  }

  @override
  void dispose() {
    player.dispose();
    super.dispose();
  }

  void _fetchMediaData() async {
    String resp = await http.read(mediaDataUrl);
    Map<String, dynamic> media = jsonDecode(resp);

    mediaDataStream.add(media);
  }

  @override
  Widget build(BuildContext context) {
    _bottomSheetMaxHeight = MediaQuery.of(context).size.height - 370;
    return Scaffold(
      backgroundColor: Color(0xFFFFD7EBFF),
      bottomSheet: _showBottomSheet(),
      floatingActionButton: _showFloatingActionButton(context),
      body: SafeArea(
        bottom: true,
        child: ListView(
          children: <Widget>[
            _showMediaSection(),
            _showInteractivityObject(),
          ],
        ),
      ),
    );
  }

  Widget _showFloatingActionButton(BuildContext context) {
    return StreamBuilder<double>(
      stream: _bottomSheetController.heightStream,
      initialData: 0,
      builder: (context, height) {
        return StreamBuilder<bool>(
          stream: creatorModeStream,
          initialData: false,
          builder: (context, creatorMode) {
            return StreamBuilder<Map<String, dynamic>>(
              stream: mediaDataStream,
              builder: (context, mediaData) {
                return Opacity(
                  opacity: creatorMode.data
                      ? 1
                      : height.data / _bottomSheetMaxHeight,
                  child: FloatingActionButton(
                    child: Icon(creatorMode.data ? Icons.headset : Icons.edit),
                    onPressed: () {
                      if (mediaData.data != null) {
                        Map<String, dynamic> _newMediaData = mediaData.data;
                        Map<String, dynamic> _newContentSections =
                            Map<String, dynamic>();
                        Map<String, dynamic> _newTriggers =
                            Map<String, dynamic>();
                        if (creatorMode.data == true) {
                          for (int i = 0;
                              i < _sectionTimestampControllers.length;
                              i++) {
                            // parse timestamp text here
                            List<String> timeUnits =
                                _sectionTimestampControllers[i].text.split(':');

                            int hours = int.parse(timeUnits[0]);
                            int minutes = int.parse(timeUnits[1]);
                            int seconds = int.parse(timeUnits[2]);
                            _newContentSections[Duration(
                                    hours: hours,
                                    minutes: minutes,
                                    seconds: seconds)
                                .inSeconds
                                .toString()] = _sectionNameControllers[i].text;
                          }
                          for (int i = 0;
                              i < _triggerTimestampControllers.length;
                              i++) {
                            // parse timestamp text here
                            List<String> timeUnits =
                                _triggerTimestampControllers[i].text.split(':');
                            Map<String, dynamic> _newTrigger =
                                Map<String, dynamic>();

                            _newTrigger["type"] =
                                _triggerTypeControllers[i].text;

                            if (_triggerTypeControllers[i].text ==
                                "display_text") {
                              _newTrigger["text"] =
                                  _triggerContentControllers[i].text;
                            } else if (_triggerTypeControllers[i].text ==
                                "display_image") {
                              _newTrigger["resource"] =
                                  _triggerContentControllers[i].text;
                            }
                            int hours = int.parse(timeUnits[0]);
                            int minutes = int.parse(timeUnits[1]);
                            int seconds = int.parse(timeUnits[2]);
                            _newTriggers[Duration(
                                    hours: hours,
                                    minutes: minutes,
                                    seconds: seconds)
                                .inSeconds
                                .toString()] = _newTrigger;
                          }
                          if (_newMediaData != {}) {
                            _newMediaData["contents"] = _newContentSections;
                            _newMediaData["triggers"] = _newTriggers;
                            mediaDataStream.add(_newMediaData);
                          }
                        }
                        creatorModeStream.add(!creatorMode.data);
                      }
                    },
                  ),
                );
              },
            );
          },
        );
      },
    );
  }

  Widget _showMediaSection() {
    return StreamBuilder<Map<String, dynamic>>(
      stream: mediaDataStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return MediaPlayer(
            player: player,
            audioUrl: snapshot.data['resource'],
            audioPositionStream: audioPositionStream,
            audioDurationStream: audioDurationStream,
            audioPlayerStateStream: audioPlayerStateStream,
          );
        } else {
          return Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 30,
                height: 30,
                child: CircularProgressIndicator(),
              ),
            ],
          );
        }
      },
    );
  }

  Container _showInteractivityObject() {
    return Container(
      height: 310,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: StreamBuilder<Widget>(
                stream: interactivityObjectStream,
                initialData: Container(),
                builder: (context, snapshot) {
                  return Container(
                    padding: EdgeInsets.all(10),
                    child: snapshot.data,
                  );
                }),
          ),
        ],
      ),
    );
  }

  Widget _showBottomSheet() {
    return SolidBottomSheet(
      controller: _bottomSheetController,
      maxHeight: _bottomSheetMaxHeight,
      headerBar: StreamBuilder<bool>(
        stream: creatorModeStream,
        initialData: false,
        builder: (context, creatorMode) {
          return Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            decoration: BoxDecoration(
              color: creatorMode.data ? Colors.red : Color(0xFF84c1ff),
            ),
            child: creatorMode.data
                ? _showCreatorModeStatus()
                : _showCurrentlyPlaying(),
          );
        },
      ),
      body: StreamBuilder<bool>(
        stream: creatorModeStream,
        initialData: false,
        builder: (context, creatorMode) {
          return StreamBuilder<bool>(
            stream: creatorModeTypeStream,
            initialData: true,
            builder: (context, type) {
              return creatorMode.data
                  ? ListView(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      padding: EdgeInsets.all(10),
                      children: <Widget>[
                        RaisedButton(
                          child: Text(
                            type.data
                                ? "Switch to canvas editor"
                                : "Switch to section editor",
                          ),
                          onPressed: () {
                            creatorModeTypeStream.add(!type.data);
                          },
                        ),
                        type.data
                            ? StreamBuilder<Map<String, dynamic>>(
                                stream: contentSectionsStream,
                                builder: (context, contentSections) {
                                  if (contentSections.data != null) {
                                    return SectionEditor(
                                      contentSectionsStream:
                                          contentSectionsStream,
                                      contentSection: contentSections.data,
                                      sectionNameControllers:
                                          _sectionNameControllers,
                                      sectionTimestampControllers:
                                          _sectionTimestampControllers,
                                      player: player,
                                      audioPositionStream: audioPositionStream,
                                    );
                                  } else {
                                    return Container();
                                  }
                                },
                              )
                            : StreamBuilder<Map<String, dynamic>>(
                                stream: triggersStream,
                                initialData: null,
                                builder: (context, _triggers) {
                                  if (_triggers.data != null &&
                                      _triggers.data[
                                              _triggers.data.keys.toList()[0]]
                                          is Map<String, dynamic>) {
                                    return CanvasEditor(
                                      triggersStream: triggersStream,
                                      triggers: _triggers.data,
                                      triggerTypeControllers:
                                          _triggerTypeControllers,
                                      triggerContentControllers:
                                          _triggerContentControllers,
                                      triggerTimestampControllers:
                                          _triggerTimestampControllers,
                                      player: player,
                                      audioPositionStream: audioPositionStream,
                                    );
                                  } else {
                                    return Container();
                                  }
                                })
                      ],
                    )
                  : SectionList(
                      player: player,
                      contentSectionsStream: contentSectionsStream,
                    );
            },
          );
        },
      ),
    );
  }

  Widget _showCreatorModeStatus() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.edit,
          color: Colors.white,
        ),
        SizedBox(width: 5),
        Text(
          "Creator mode is enabled",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }

  Widget _showCurrentlyPlaying() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.headset,
          color: Colors.white,
        ),
        SizedBox(width: 5),
        Text(
          "Currently Playing",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
            fontSize: 14,
          ),
        ),
        Text(
          " - ",
          style: TextStyle(color: Colors.white, fontSize: 14),
        ),
        StreamBuilder<Map<String, dynamic>>(
          stream: contentSectionsStream,
          builder: (context, contentSections) {
            return StreamBuilder<int>(
              stream: sectionStream,
              initialData: 0,
              builder: (context, section) {
                return Text(
                  (contentSections.data != null)
                      ? contentSections.data[
                          contentSections.data.keys.toList()[section.data]]
                      : "Loading..",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                );
              },
            );
          },
        ),
      ],
    );
  }
}
