import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:audioplayers/audioplayers.dart';

class CanvasEditor extends StatefulWidget {
  const CanvasEditor({
    Key key,
    @required this.triggersStream,
    @required this.triggers,
    @required List<TextEditingController> triggerTypeControllers,
    @required List<TextEditingController> triggerContentControllers,
    @required List<TextEditingController> triggerTimestampControllers,
    @required this.player,
    @required this.audioPositionStream,
  })  : _triggerTypeControllers = triggerTypeControllers,
        _triggerContentControllers = triggerContentControllers,
        _triggerTimestampControllers = triggerTimestampControllers,
        super(key: key);

  final BehaviorSubject<Map<String, dynamic>> triggersStream;
  final Map<String, dynamic> triggers;
  final List<TextEditingController> _triggerTypeControllers;
  final List<TextEditingController> _triggerContentControllers;
  final List<TextEditingController> _triggerTimestampControllers;
  final AudioPlayer player;
  final BehaviorSubject<Duration> audioPositionStream;

  @override
  _CanvasEditorState createState() => _CanvasEditorState();
}

class _CanvasEditorState extends State<CanvasEditor> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Form(
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: widget.triggers.length,
            itemBuilder: (context, index) {
              return Row(
                children: <Widget>[
                  SizedBox(
                    width: 40,
                    child: RaisedButton(
                      padding: EdgeInsets.all(0),
                      child: widget._triggerTypeControllers[index].text ==
                              "display_text"
                          ? Icon(Icons.text_fields)
                          : Icon(Icons.image),
                      onPressed: () {
                        if (widget._triggerTypeControllers[index].text ==
                            "display_text") {
                          setState(() {
                            widget._triggerTypeControllers[index].text =
                                "display_image";
                          });
                        } else {
                          setState(() {
                            widget._triggerTypeControllers[index].text =
                                "display_text";
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 3,
                    child: TextFormField(
                      controller: widget._triggerContentControllers[index],
                    ),
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 90,
                    child: TextFormField(
                      controller: widget._triggerTimestampControllers[index],
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 40,
                    child: RaisedButton(
                      color: Colors.blue,
                      padding: EdgeInsets.all(0),
                      child: Icon(Icons.play_arrow, color: Colors.white),
                      onPressed: () {
                        widget.player.seek(
                          Duration(
                            seconds: int.parse(
                              widget.triggers.keys.toList()[index],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 40,
                    child: RaisedButton(
                      color: Colors.red,
                      padding: EdgeInsets.all(0),
                      child: Icon(Icons.close, color: Colors.white),
                      onPressed: () {
                        Map<String, dynamic> _newTriggers = widget.triggers;
                        _newTriggers
                            .remove(widget.triggers.keys.toList()[index]);
                        widget.triggersStream.add(_newTriggers);
                      },
                    ),
                  )
                ],
              );
            },
          ),
        ),
        SizedBox(height: 20),
        Row(
          children: <Widget>[
            Expanded(
              child: StreamBuilder<Duration>(
                stream: widget.audioPositionStream,
                builder: (context, snapshot) {
                  return RaisedButton(
                    child: Text("Add new trigger"),
                    onPressed: () {
                      Map<String, dynamic> _newTriggers = widget.triggers;
                      Map<String, dynamic> _newTrigger = Map<String, dynamic>();
                      _newTrigger["type"] = "display_text";
                      _newTrigger["text"] = "Untitled Section";
                      _newTriggers[snapshot.data.inSeconds.toString()] =
                          _newTrigger;
                      SplayTreeMap<String, dynamic> sortedTriggers =
                          SplayTreeMap<String, dynamic>.from(
                        _newTriggers,
                        (a, b) => int.parse(a).compareTo(
                          int.parse(b),
                        ),
                      );
                      widget.triggersStream.add(sortedTriggers);
                    },
                  );
                },
              ),
            )
          ],
        )
      ],
    );
  }
}
