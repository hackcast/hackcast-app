import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:rxdart/rxdart.dart';

class MediaPlayer extends StatefulWidget {
  final AudioPlayer player;
  final String audioUrl;
  final PublishSubject<Duration> audioDurationStream;
  final BehaviorSubject<Duration> audioPositionStream;
  final PublishSubject<AudioPlayerState> audioPlayerStateStream;

  const MediaPlayer({
    Key key,
    this.player,
    this.audioUrl,
    this.audioDurationStream,
    this.audioPositionStream,
    this.audioPlayerStateStream,
  }) : super(key: key);

  @override
  _MediaPlayerState createState() => _MediaPlayerState();
}

class _MediaPlayerState extends State<MediaPlayer> {
  @override
  void initState() {
    super.initState();

    if (widget.audioUrl != null) {
      widget.player.play(widget.audioUrl);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: 20),
      children: <Widget>[
        SizedBox(height: 20),
        _showMediaInfo(),
        _showSlider(context),
        _showMediaControl(),
        SizedBox(height: 20),
      ],
    );
  }

  Row _showMediaInfo() {
    return Row(
      children: <Widget>[
        Container(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Image.asset(
              'assets/artwork.png',
              width: 100,
            ),
          ),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(50, 0, 0, 0),
                blurRadius: 32,
                offset: Offset(0, 0),
              ),
            ],
          ),
        ),
        SizedBox(width: 20),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "HackLodge Podcast",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "Demo #1",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _showSlider(BuildContext context) {
    return StreamBuilder<Duration>(
      stream: widget.audioDurationStream,
      initialData: Duration(seconds: 0),
      builder: (context, duration) {
        return StreamBuilder<Duration>(
          stream: widget.audioPositionStream,
          initialData: Duration(seconds: 0),
          builder: (context, position) {
            return StreamBuilder<AudioPlayerState>(
              stream: widget.audioPlayerStateStream,
              initialData: AudioPlayerState.PLAYING,
              builder: (context, playerState) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 40,
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          trackHeight: 4,
                          trackShape: CustomTrackShape(),
                          thumbColor: Color(0xFF84c1ff),
                          activeTrackColor: Color(0xFF84c1ff),
                          valueIndicatorColor: Color(0xFFadd6ff),
                          thumbShape:
                              RoundSliderThumbShape(enabledThumbRadius: 6),
                        ),
                        child: Transform.translate(
                          offset: Offset(0, -8),
                          child: Slider(
                            value: position.data.inSeconds.toDouble(),
                            min: 0.0,
                            max: duration.data.inSeconds.toDouble() + 1,
                            onChanged: (double value) {
                              if (playerState.data ==
                                      AudioPlayerState.PLAYING ||
                                  playerState.data == AudioPlayerState.PAUSED) {
                                widget.player
                                    .seek(Duration(seconds: value.toInt()));
                              } else if (playerState.data ==
                                  AudioPlayerState.COMPLETED) {
                                widget.player.play(widget.audioUrl);
                                widget.player
                                    .seek(Duration(seconds: value.toInt()));
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                    Text(
                      position.data.toString().split('.').first.padLeft(8, "0"),
                    ),
                  ],
                );
              },
            );
          },
        );
      },
    );
  }

  Widget _showMediaControl() {
    return StreamBuilder<Duration>(
        stream: widget.audioDurationStream,
        initialData: Duration(seconds: 0),
        builder: (context, duration) {
          return StreamBuilder<Duration>(
              stream: widget.audioPositionStream,
              initialData: Duration(seconds: 0),
              builder: (context, position) {
                return StreamBuilder<AudioPlayerState>(
                    stream: widget.audioPlayerStateStream,
                    initialData: AudioPlayerState.PLAYING,
                    builder: (context, playerState) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          GestureDetector(
                            child: Icon(
                              Icons.navigate_before,
                              color: Color(0xFF003163),
                              size: 65,
                            ),
                            onTap: () {
                              if (playerState.data ==
                                  AudioPlayerState.COMPLETED) {
                                widget.player.play(widget.audioUrl);
                              }

                              if (position.data - Duration(seconds: 15) <
                                  Duration(seconds: 0)) {
                                widget.player.seek(Duration(seconds: 0));
                              } else {
                                widget.player.seek(
                                    position.data - Duration(seconds: 15));
                              }
                            },
                          ),
                          GestureDetector(
                            child: Icon(
                              playerState.data == AudioPlayerState.PLAYING
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              color: Color(0xFF003163),
                              size: 65,
                            ),
                            onTap: () {
                              if (playerState.data ==
                                  AudioPlayerState.PLAYING) {
                                widget.player.pause();
                              } else if (playerState.data ==
                                  AudioPlayerState.PAUSED) {
                                widget.player.resume();
                              } else if (playerState.data ==
                                  AudioPlayerState.COMPLETED) {
                                widget.player.play(widget.audioUrl);
                              }
                            },
                          ),
                          GestureDetector(
                            child: Icon(
                              Icons.navigate_next,
                              color: Color(0xFF003163),
                              size: 65,
                            ),
                            onTap: () {
                              if (position.data + Duration(seconds: 15) >
                                  duration.data) {
                                widget.player.seek(position.data +
                                    (duration.data) -
                                    position.data);
                              } else {
                                widget.player.seek(
                                    position.data + Duration(seconds: 15));
                              }
                            },
                          ),
                        ],
                      );
                    });
              });
        });
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  Rect getPreferredRect({
    @required RenderBox parentBox,
    Offset offset = Offset.zero,
    @required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = sliderTheme.trackHeight;
    final double trackLeft = offset.dx;
    final double trackTop = offset.dy + parentBox.size.height - trackHeight;
    final double trackWidth = parentBox.size.width;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}
