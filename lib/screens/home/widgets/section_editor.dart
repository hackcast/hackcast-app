import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:audioplayers/audioplayers.dart';

class SectionEditor extends StatelessWidget {
  const SectionEditor({
    Key key,
    @required this.contentSectionsStream,
    @required this.contentSection,
    @required List<TextEditingController> sectionNameControllers,
    @required List<TextEditingController> sectionTimestampControllers,
    @required this.player,
    @required this.audioPositionStream,
  })  : _sectionNameControllers = sectionNameControllers,
        _sectionTimestampControllers = sectionTimestampControllers,
        super(key: key);

  final BehaviorSubject<Map<String, dynamic>> contentSectionsStream;
  final Map<String, dynamic> contentSection;
  final List<TextEditingController> _sectionNameControllers;
  final List<TextEditingController> _sectionTimestampControllers;
  final AudioPlayer player;
  final BehaviorSubject<Duration> audioPositionStream;

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Form(
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: contentSection.length,
            itemBuilder: (context, index) {
              return Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: TextFormField(
                      controller: _sectionNameControllers[index],
                    ),
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 90,
                    child: TextFormField(
                      controller: _sectionTimestampControllers[index],
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 40,
                    child: RaisedButton(
                      color: Colors.blue,
                      padding: EdgeInsets.all(0),
                      child: Icon(Icons.play_arrow, color: Colors.white),
                      onPressed: () {
                        player.seek(
                          Duration(
                            seconds: int.parse(
                              contentSection.keys.toList()[index],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(width: 10),
                  SizedBox(
                    width: 40,
                    child: RaisedButton(
                      color: Colors.red,
                      padding: EdgeInsets.all(0),
                      child: Icon(Icons.close, color: Colors.white),
                      onPressed: () {
                        Map<String, dynamic> _newContentSection =
                            contentSection;
                        _newContentSection
                            .remove(contentSection.keys.toList()[index]);
                        contentSectionsStream.add(_newContentSection);
                      },
                    ),
                  )
                ],
              );
            },
          ),
        ),
        SizedBox(height: 20),
        Row(
          children: <Widget>[
            Expanded(
              child: StreamBuilder<Duration>(
                stream: audioPositionStream,
                builder: (context, snapshot) {
                  return RaisedButton(
                    child: Text("Add new section marker"),
                    onPressed: () {
                      Map<String, dynamic> _newContentSection = contentSection;
                      _newContentSection[snapshot.data.inSeconds.toString()] =
                          "Untitled Section";
                      SplayTreeMap<String, dynamic> sortedSections =
                          SplayTreeMap<String, dynamic>.from(
                        _newContentSection,
                        (a, b) => int.parse(a).compareTo(
                          int.parse(b),
                        ),
                      );
                      contentSectionsStream.add(sortedSections);
                    },
                  );
                },
              ),
            )
          ],
        )
      ],
    );
  }
}
