import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:rxdart/rxdart.dart';

class SectionList extends StatelessWidget {
  const SectionList({
    Key key,
    @required this.player,
    @required this.contentSectionsStream,
  }) : super(key: key);

  final AudioPlayer player;
  final BehaviorSubject<Map<String, dynamic>> contentSectionsStream;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Map<String, dynamic>>(
      stream: contentSectionsStream,
      builder: (context, contentSections) {
        if (contentSections.data != null) {
          List<String> contentTimestamps = contentSections.data.keys.toList();
          return ListView(
            children: <Widget>[
              ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                separatorBuilder: (context, index) => Divider(
                  color: Colors.grey,
                  height: 0,
                ),
                itemCount: contentTimestamps.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    onTap: () {
                      player.seek(
                        Duration(
                          seconds: int.parse(
                            contentTimestamps[index],
                          ),
                        ),
                      );
                    },
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          contentSections.data[contentTimestamps[index]],
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          Duration(
                            seconds: int.parse(
                              contentTimestamps[index],
                            ),
                          ).toString().split('.').first.padLeft(8, "0"),
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.blueGrey,
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              SizedBox(height: 10),
            ],
          );
        } else {
          return Container();
        }
      },
    );
  }
}
